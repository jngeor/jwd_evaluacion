package com.javaweb.clinica.usuario;

import com.javaweb.clinica.usuario.application.IUsuarioService;
import com.javaweb.clinica.usuario.domain.Usuario;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ContextConfiguration
@Transactional
public class UsuarioServiceTest {

    @Autowired
    IUsuarioService usuarioService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Test
    @Commit
    public void registrarCliente() {
        Usuario usuario =  new Usuario();
        usuario.setNombre("Walter Pantigoso");
        usuario.setDocumento("00403881");
        usuario.setClave(passwordEncoder.encode("123456"));
        usuario.setRol("Administrador");

        usuarioService.grabarUsuario(usuario);

        assertTrue(1 > 0, "No se registro el cliente");
    }

}
