package com.javaweb.clinica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JwdEvaluacionApplication {

	public static void main(String[] args) {
		SpringApplication.run(JwdEvaluacionApplication.class, args);
	}

}
