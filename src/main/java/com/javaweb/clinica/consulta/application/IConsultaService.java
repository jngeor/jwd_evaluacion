package com.javaweb.clinica.consulta.application;

import com.javaweb.clinica.consulta.domain.Consulta;
import com.javaweb.clinica.consulta.domain.Historia;
import com.javaweb.clinica.doctor.domain.Doctor;
import com.javaweb.clinica.paciente.domain.Paciente;

import java.util.List;

public interface IConsultaService {

    public List<Consulta> listarConsulta();
    public Consulta obtenerConsulta(Integer id);
    public Consulta grabarConsulta(Consulta consulta);
    public int eliminarConsulta(Integer id);
    public List<Consulta> buscarPorDoctor(Doctor doctor);
    public List<Consulta> buscarPorPaciente(Paciente paciente);
    public List<Historia> listarHistorias();

}
