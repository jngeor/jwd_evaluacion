package com.javaweb.clinica.consulta.application;

import com.javaweb.clinica.consulta.domain.Consulta;
import com.javaweb.clinica.consulta.domain.Historia;
import com.javaweb.clinica.consulta.infraestructura.repository.ConsultaRepository;
import com.javaweb.clinica.doctor.domain.Doctor;
import com.javaweb.clinica.paciente.domain.Paciente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ConsultaService implements IConsultaService {

    @Autowired
    ConsultaRepository consultaRepository;

    @Override
    public List<Consulta> listarConsulta() {
        return consultaRepository.findAll();
    }

    @Override
    public Consulta obtenerConsulta(Integer id) {
        return consultaRepository.findById(id).orElse(null);
    }

    @Override
    public Consulta grabarConsulta(Consulta consulta) {
        return consultaRepository.save(consulta);
    }

    @Override
    public int eliminarConsulta(Integer id) {
        consultaRepository.deleteById(id);
        return 1;
    }

    @Override
    public List<Consulta> buscarPorDoctor(Doctor doctor) {
        return consultaRepository.findByDoctor(doctor);
    }

    @Override
    public List<Consulta> buscarPorPaciente(Paciente paciente) {
        return consultaRepository.findByPaciente(paciente);
    }

    @Override
    public List<Historia> listarHistorias() {
        List<Historia> lista = new ArrayList<>();
        List<Consulta> consultas = consultaRepository.findAll();
        consultas.forEach(consulta -> {
            Historia historia = new Historia();
            historia.setPacienteNombre(consulta.getPaciente().getNombre());
            historia.setDoctorNombre(consulta.getDoctor().getNombre());
            historia.setEspecialidad(consulta.getDoctor().getEspecialidad().getNombre());
            historia.setFecha(consulta.getFecha().toString());
            lista.add(historia);
        });

        return lista;
    }
}
