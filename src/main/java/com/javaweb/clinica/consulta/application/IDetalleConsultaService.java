package com.javaweb.clinica.consulta.application;

import com.javaweb.clinica.consulta.domain.Consulta;
import com.javaweb.clinica.consulta.domain.DetalleConsulta;

public interface IDetalleConsultaService {

    public DetalleConsulta findById(Integer id);
    public DetalleConsulta findByConsulta(Consulta consulta);
    public DetalleConsulta grabarDetalleConsulta(DetalleConsulta detalleConsulta);

}
