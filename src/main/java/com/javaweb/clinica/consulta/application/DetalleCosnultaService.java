package com.javaweb.clinica.consulta.application;

import com.javaweb.clinica.consulta.domain.Consulta;
import com.javaweb.clinica.consulta.domain.DetalleConsulta;
import com.javaweb.clinica.consulta.infraestructura.repository.DetalleConsultaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DetalleCosnultaService implements  IDetalleConsultaService {

    @Autowired
    DetalleConsultaRepository detalleConsultaRepository;

    @Override
    public DetalleConsulta findById(Integer id) {
        return detalleConsultaRepository.findById(id).orElse(null);
    }

    @Override
    public DetalleConsulta findByConsulta(Consulta consulta) {
        return detalleConsultaRepository.findByConsulta(consulta);
    }

    @Override
    public DetalleConsulta grabarDetalleConsulta(DetalleConsulta detalleConsulta) {
        return detalleConsultaRepository.save(detalleConsulta);
    }

}
