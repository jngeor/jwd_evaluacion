package com.javaweb.clinica.consulta.domain;

import lombok.Data;

@Data
public class Historia {

    public String pacienteNombre;
    public String doctorNombre;
    public String especialidad;
    public String fecha;

}
