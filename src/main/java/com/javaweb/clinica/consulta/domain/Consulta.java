package com.javaweb.clinica.consulta.domain;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.javaweb.clinica.doctor.domain.Doctor;
import com.javaweb.clinica.paciente.domain.Paciente;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name="medical_consultation")
public class Consulta {

    @NonNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "serial")
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_doctor", updatable = false)
    @JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
    private Doctor doctor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_patient", updatable = false)
    @JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
    private Paciente paciente;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @NotNull
    @Column(name = "create_date")
    @JsonProperty(value = "create_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate fecha;

}
