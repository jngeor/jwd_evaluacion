package com.javaweb.clinica.consulta.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name="detail_consultation")
public class DetalleConsulta {

    @NonNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "serial")
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_medical_consultation", updatable = false)
    @JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
    private Consulta consulta;

    @NonNull
    @Column(name = "diagnostic")
    private String diagnostico;

    @NonNull
    @Column(name = "treatment")
    private String tratamiento;

}
