package com.javaweb.clinica.consulta.infraestructura.repository;

import com.javaweb.clinica.consulta.domain.Consulta;
import com.javaweb.clinica.doctor.domain.Doctor;
import com.javaweb.clinica.paciente.domain.Paciente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConsultaRepository extends JpaRepository<Consulta, Integer> {

    List<Consulta> findByDoctor(Doctor doctor);

    List<Consulta> findByPaciente(Paciente paciente);

}
