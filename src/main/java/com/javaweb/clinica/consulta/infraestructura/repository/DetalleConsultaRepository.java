package com.javaweb.clinica.consulta.infraestructura.repository;

import com.javaweb.clinica.consulta.domain.Consulta;
import com.javaweb.clinica.consulta.domain.DetalleConsulta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DetalleConsultaRepository extends JpaRepository<DetalleConsulta, Integer> {

    DetalleConsulta findByConsulta(Consulta consulta);

}
