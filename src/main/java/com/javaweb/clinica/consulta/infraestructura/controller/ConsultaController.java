package com.javaweb.clinica.consulta.infraestructura.controller;

import com.javaweb.clinica.consulta.application.IConsultaService;
import com.javaweb.clinica.consulta.domain.Consulta;
import com.javaweb.clinica.doctor.application.IDoctorService;
import com.javaweb.clinica.doctor.domain.Doctor;
import com.javaweb.clinica.paciente.application.IPacienteService;
import com.javaweb.clinica.paciente.domain.Paciente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;

@Controller
@RequestMapping("/consulta")
public class ConsultaController {

    @Autowired
    IDoctorService doctorService;

    @Autowired
    IPacienteService pacienteService;

    @Autowired
    IConsultaService consultaService;

    @GetMapping("/crear")
    public String crear(ModelMap model)
    {
        Collection<Doctor> doctores = doctorService.listarDoctores();
        Collection<Paciente> pacientes = pacienteService.listarPacientes();

        model.put("doctores", doctores);
        model.put("pacientes", pacientes);

        return "consulta/crear";
    }

    @RequestMapping(value = {"/crear"}, method = RequestMethod.POST)
    public String crear(@RequestParam Integer idDoctor,
                        @RequestParam Integer idPaciente,
                        @RequestParam String fecha,
                        ModelMap model)
    {
        Doctor doctor = doctorService.obtenerDoctor(idDoctor);
        Paciente paciente = pacienteService.obtenerPaciente(idPaciente);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate localDate = LocalDate.parse(fecha, formatter);
        String result = "";

        Consulta consulta = new Consulta();
        consulta.setDoctor(doctor);
        consulta.setPaciente(paciente);
        consulta.setFecha(localDate);

        consulta = consultaService.grabarConsulta(consulta);

        if (consulta != null)
        {
            result = "redirect:/consulta/buscar";
        }
        else
        {
            Collection<Doctor> doctores = doctorService.listarDoctores();
            Collection<Paciente> pacientes = pacienteService.listarPacientes();

            model.put("doctores", doctores);
            model.put("pacientes", pacientes);
            model.put("mensaje", "Ocurrio un error al guardar");
            result = "consulta/crear";
        }

        return result;
    }

    @GetMapping("/buscar")
    public String buscar(ModelMap model)
    {
        Collection<Consulta> consultas = consultaService.listarConsulta();
        model.put("consultas", consultas);

        return "consulta/buscar";
    }

}
