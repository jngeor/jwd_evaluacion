package com.javaweb.clinica.paciente.domain;

import javax.persistence.*;

import lombok.*;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name="patient")
public class Paciente {

    @NonNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "serial")
    private Integer id;

    @NonNull
    @Column(name = "first_name")
    private String primerNombre;

    @NonNull
    @Column(name = "last_name")
    private String segundoNombre;

    @NonNull
    @Column(name = "dni")
    private String dni;

    @NonNull
    @Column(name = " number_clinical_history")
    private String numeroHistoria;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    @Transient
    private String nombre;

    public String getNombre()
    {
        return segundoNombre + ", " + primerNombre;
    }

}
