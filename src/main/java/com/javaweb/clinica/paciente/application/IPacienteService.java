package com.javaweb.clinica.paciente.application;

import com.javaweb.clinica.paciente.domain.Paciente;

import java.util.Collection;

public interface IPacienteService {

    public Collection<Paciente> listarPacientes();
    public Paciente obtenerPaciente(int id);
    public Paciente insertarPaciente(Paciente doctor);
    public Integer eliminarById(int id);

}
