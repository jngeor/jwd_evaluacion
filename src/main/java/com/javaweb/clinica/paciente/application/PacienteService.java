package com.javaweb.clinica.paciente.application;

import com.javaweb.clinica.paciente.domain.Paciente;
import com.javaweb.clinica.paciente.infraestructure.repository.PacienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class PacienteService implements IPacienteService {

    @Autowired
    PacienteRepository pacienteRepository;

    @Override
    public Collection<Paciente> listarPacientes() {
        return pacienteRepository.findAll(Sort.by("id"));
    }

    @Override
    public Paciente obtenerPaciente(int id) {
        return pacienteRepository.findById(id).orElse(null);
    }

    @Override
    public Paciente insertarPaciente(Paciente doctor) {
        return pacienteRepository.save(doctor);
    }

    @Override
    public Integer eliminarById(int id) {
        pacienteRepository.deleteById(id);
        return 1;
    }

}
