package com.javaweb.clinica.paciente.infraestructure.controller;

import com.javaweb.clinica.configuracion.Response;
import com.javaweb.clinica.paciente.application.IPacienteService;
import com.javaweb.clinica.paciente.domain.Paciente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@Controller
@RequestMapping("/paciente")
public class PacienteController {

    @Autowired
    private IPacienteService pacienteService;

    @GetMapping("/resumen")
    public String resumen(ModelMap model)
    {
        Collection<Paciente> pacientes = pacienteService.listarPacientes();
        model.put("listapacientes", pacientes);
        return "paciente/resumen";
    }

    @GetMapping("/agregar")
    public String agregar(ModelMap model)
    {
        return "paciente/agregar";
    }

    @RequestMapping(value = {"/agregar"}, method = RequestMethod.POST)
    public String agregar(@RequestParam String primernombre,
                          @RequestParam String segundonombre,
                          @RequestParam String dni,
                          @RequestParam String historia,
                          ModelMap model)
    {
        Paciente paciente = new Paciente();
        String result = "";

        paciente.setPrimerNombre(primernombre);
        paciente.setSegundoNombre(segundonombre);
        paciente.setDni(dni);
        paciente.setNumeroHistoria(historia);

        paciente = pacienteService.insertarPaciente(paciente);

        if (paciente != null)
        {
            result = "redirect:/paciente/resumen";
        }
        else
        {
            model.put("mensaje", "Ocurrio un error al guardar");
            return "paciente/agregar";
        }

        return result;
    }

    @GetMapping("/modificar/{id}")
    public String modificar(@PathVariable(value = "id") Integer id, ModelMap model)
    {
        Paciente paciente = pacienteService.obtenerPaciente(id);
        model.put("paciente", paciente);
        return "paciente/modificar";
    }

    @RequestMapping(value = {"/modificar"}, method = RequestMethod.POST)
    public String modificar(@RequestParam Integer id,
                            @RequestParam String primernombre,
                            @RequestParam String segundonombre,
                            @RequestParam String dni,
                            @RequestParam String historia,
                            ModelMap model)
    {
        Paciente paciente = new Paciente();
        String result = "";

        paciente.setId(id);
        paciente.setPrimerNombre(primernombre);
        paciente.setSegundoNombre(segundonombre);
        paciente.setDni(dni);
        paciente.setNumeroHistoria(historia);

        paciente = pacienteService.insertarPaciente(paciente);

        if (paciente != null)
        {
            result = "redirect:/paciente/resumen";
        }
        else
        {
            model.put("mensaje", "Ocurrio un error al guardar");
            return "paciente/modificar";
        }

        return result;
    }

    @PostMapping("/eliminar/{id}")
    @ResponseBody
    public ResponseEntity<Response> eliminarpaciengte(@PathVariable(value = "id") Integer id)
    {
        ResponseEntity<Response> response = null;
        int result = pacienteService.eliminarById(id);

        if (result == 1) {
            response = new ResponseEntity<Response>(new Response("Se elimino correctamemte"),
                    HttpStatus.OK);
        } else {
            response = new ResponseEntity<Response>(new Response("No se elimino"),
                    HttpStatus.EXPECTATION_FAILED);
        }

        return response;
    }

}
