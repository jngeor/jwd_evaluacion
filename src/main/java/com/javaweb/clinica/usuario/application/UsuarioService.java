package com.javaweb.clinica.usuario.application;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;

import com.javaweb.clinica.usuario.domain.Usuario;
import com.javaweb.clinica.usuario.infraestructure.repository.UsuarioRepository;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService implements IUsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Override
	public Collection<Usuario> listarUsuarios() {
		return usuarioRepository.findAll();
	}

	@Override
	public Usuario obtenerUsuario(String documento) {
		return usuarioRepository.findByDocumento(documento);
	}

	@Override
	public Usuario validarUsuario(String documento, String clave) {
		return usuarioRepository.findByDocumentoAndClave(documento, clave);
	}

	@Override
	public Usuario grabarUsuario(Usuario usuario) {
		return usuarioRepository.save(usuario);
	}

}
