package com.javaweb.clinica.usuario.application;

import java.util.Collection;

import com.javaweb.clinica.usuario.domain.Usuario;

public interface IUsuarioService {
	
	public Collection<Usuario> listarUsuarios();
	public Usuario obtenerUsuario(String documento);
	public Usuario validarUsuario(String documento, String clave);
	public Usuario grabarUsuario(Usuario usuario);
	
}
