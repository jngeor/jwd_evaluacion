package com.javaweb.clinica.usuario.infraestructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.javaweb.clinica.usuario.domain.Usuario;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

	Usuario findByDocumento(String documento);

	Usuario findByDocumentoAndClave(String documento, String clave);

}
