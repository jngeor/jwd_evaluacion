package com.javaweb.clinica.configuracion;

import com.javaweb.clinica.usuario.application.IUsuarioService;
import com.javaweb.clinica.usuario.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Component
public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {

    @Autowired
    private IUsuarioService usuarioService;

    @Autowired
    private HttpSession session;

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    enum ROLES {
        ADMIN, USER;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {

        String username = ((User) authentication.getPrincipal()).getUsername();
        Usuario usuario = usuarioService.obtenerUsuario(username);

        if(usuario != null) {
            session.setAttribute("usuario", usuario);
        }
        String url = "/inicio";

        redirectStrategy.sendRedirect(request, response, url);
    }

}
