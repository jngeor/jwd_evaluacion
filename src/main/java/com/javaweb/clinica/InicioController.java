package com.javaweb.clinica;

import com.javaweb.clinica.usuario.application.IUsuarioService;
import com.javaweb.clinica.usuario.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.Optional;

@Controller
public class InicioController {

    @Autowired
    private IUsuarioService usuarioService;

    @RequestMapping(value = {"/", "/inicio"}, method = RequestMethod.GET)
    public String inicio()
    {
        return "/inicio";
    }

    @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
    public String login()
    {
        return "login";
    }

    @RequestMapping(value = {"/login"}, method = RequestMethod.POST)
    public String login(ModelMap model, @RequestParam String username, @RequestParam String password, HttpSession session)
    {
        String result;
        Optional<Usuario> usuario = Optional.ofNullable(usuarioService.validarUsuario(username, password));

        if(usuario.isPresent())
        {
            result = "redirect:/inicio";
            session.setAttribute("usuario", usuario.get());
        }
        else
        {
            model.put("mensaje", "Credenciales no validas");
            result = "login";
        }

        return result;
    }

	@RequestMapping(value = { "/logout" }, method = RequestMethod.GET)
	public String logout(HttpSession session) {
		session.removeAttribute("usuario");

		return "redirect:/login";
	}

}
