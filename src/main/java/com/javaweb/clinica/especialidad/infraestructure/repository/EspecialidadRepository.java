package com.javaweb.clinica.especialidad.infraestructure.repository;

import com.javaweb.clinica.especialidad.domain.Especialidad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface EspecialidadRepository  extends JpaRepository<Especialidad, Integer> {

}
