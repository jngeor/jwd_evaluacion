package com.javaweb.clinica.especialidad.infraestructure.controller;

import com.javaweb.clinica.configuracion.Response;
import com.javaweb.clinica.especialidad.application.IEspecialidadService;
import com.javaweb.clinica.especialidad.domain.Especialidad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@Controller
@RequestMapping("/especialidad")
public class EspecialidadController {

    @Autowired
    IEspecialidadService especialidadService;

    @GetMapping("/resumen")
    public String resumenEspecialidad(ModelMap model)
    {
        Collection<Especialidad> especialidades = especialidadService.listarEspecialidades();
        model.put("listaespecialidad", especialidades);
        return "especialidad/resumen";
    }

    @PostMapping("/insertar/{nombre}")
    @ResponseBody
    public ResponseEntity<Response> insertarEspecialidad(@PathVariable(value = "nombre") String nombre)
    {
        ResponseEntity<Response> response = null;
        Especialidad especialidad = new Especialidad();
        especialidad.setNombre(nombre);
        especialidad = especialidadService.insertarEspecialidad(especialidad);

        if (especialidad != null) {
            response = new ResponseEntity<Response>(new Response("Se agrego correctamemte"),
                    HttpStatus.OK);
        } else {
            response = new ResponseEntity<Response>(new Response("No se agrego"),
                    HttpStatus.EXPECTATION_FAILED);
        }

        return response;
    }

    @PostMapping("/modificar/{nombre}/{id}")
    @ResponseBody
    public ResponseEntity<Response> modificarEspecialidad(@PathVariable(value = "nombre") String nombre, @PathVariable(value = "id") Integer id)
    {
        ResponseEntity<Response> response = null;
        Especialidad especialidad = especialidadService.obtenerEspecialidad(id);
        especialidad.setNombre(nombre);
        especialidad = especialidadService.insertarEspecialidad(especialidad);

        if (especialidad != null) {
            response = new ResponseEntity<Response>(new Response("Se agrego modifico"),
                    HttpStatus.OK);
        } else {
            response = new ResponseEntity<Response>(new Response("No se modifico"),
                    HttpStatus.EXPECTATION_FAILED);
        }

        return response;
    }

    @PostMapping("/eliminar/{id}")
    @ResponseBody
    public ResponseEntity<Response> eliminarEspecialidad(@PathVariable(value = "id") Integer id)
    {
        ResponseEntity<Response> response = null;
        int result = especialidadService.eliminarById(id);

        if (result == 1) {
            response = new ResponseEntity<Response>(new Response("Se elimino correctamemte"),
                    HttpStatus.OK);
        } else {
            response = new ResponseEntity<Response>(new Response("No se elimino"),
                    HttpStatus.EXPECTATION_FAILED);
        }

        return response;
    }

}
