package com.javaweb.clinica.especialidad.application;

import com.javaweb.clinica.especialidad.domain.Especialidad;
import com.javaweb.clinica.especialidad.infraestructure.repository.EspecialidadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class EspecialidadService implements IEspecialidadService {

    @Autowired
    EspecialidadRepository especialidadRepository;

    @Override
    public Collection<Especialidad> listarEspecialidades() {
        return especialidadRepository.findAll();
    }

    @Override
    public Especialidad obtenerEspecialidad(int id) {
        return especialidadRepository.findById(id).orElse(null);
    }

    @Override
    public Especialidad insertarEspecialidad(Especialidad especialidad) {
        return especialidadRepository.save(especialidad);
    }

    @Override
    public Integer eliminarById(int id) {
        especialidadRepository.deleteById(id);
        return 1;
    }
}
