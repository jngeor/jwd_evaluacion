package com.javaweb.clinica.especialidad.application;

import com.javaweb.clinica.especialidad.domain.Especialidad;

import java.util.Collection;

public interface IEspecialidadService {

    public Collection<Especialidad> listarEspecialidades();
    public Especialidad obtenerEspecialidad(int id);
    public Especialidad insertarEspecialidad(Especialidad especialidad);
    public Integer eliminarById(int id);

}
