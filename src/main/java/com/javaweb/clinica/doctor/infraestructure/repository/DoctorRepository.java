package com.javaweb.clinica.doctor.infraestructure.repository;

import com.javaweb.clinica.doctor.domain.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Integer> {
}
