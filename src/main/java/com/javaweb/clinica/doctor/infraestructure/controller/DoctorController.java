package com.javaweb.clinica.doctor.infraestructure.controller;

import com.javaweb.clinica.configuracion.Response;
import com.javaweb.clinica.doctor.application.IDoctorService;
import com.javaweb.clinica.doctor.domain.Doctor;
import com.javaweb.clinica.especialidad.application.IEspecialidadService;
import com.javaweb.clinica.especialidad.domain.Especialidad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@Controller
@RequestMapping("/doctor")
public class DoctorController {

    @Autowired
    IDoctorService doctorService;

    @Autowired
    IEspecialidadService especialidadService;

    @GetMapping("/resumen")
    public String resumen(ModelMap model)
    {
        Collection<Doctor> doctores = doctorService.listarDoctores();
        model.put("listadoctores", doctores);
        return "doctor/resumen";
    }

    @GetMapping("/agregar")
    public String agregar(ModelMap model)
    {
        Collection<Especialidad> especialidades = especialidadService.listarEspecialidades();
        model.put("especialidades", especialidades);
        return "doctor/agregar";
    }

    @RequestMapping(value = {"/agregar"}, method = RequestMethod.POST)
    public String agregar(@RequestParam Integer especialidad,
                          @RequestParam String primernombre,
                          @RequestParam String segundonombre,
                          @RequestParam String dni,
                          @RequestParam String cmp,
                          ModelMap model)
    {
        Doctor doctor = new Doctor();
        String result = "";
        Especialidad especialidadSolicitada = especialidadService.obtenerEspecialidad(especialidad);

        doctor.setPrimerNombre(primernombre);
        doctor.setSegundoNombre(segundonombre);
        doctor.setEspecialidad(especialidadSolicitada);
        doctor.setDni(dni);
        doctor.setCmp(cmp);

        doctor = doctorService.insertarDoctor(doctor);

        if (doctor != null)
        {
            result = "redirect:/doctor/resumen";
        }
        else
        {
            Collection<Especialidad> especialidades = especialidadService.listarEspecialidades();
            model.put("especialidades", especialidades);
            model.put("mensaje", "Ocurrio un error al guardar");
            result = "doctor/agregar";
        }

        return result;
    }

    @GetMapping("/modificar/{id}")
    public String modificar(@PathVariable(value = "id") Integer id, ModelMap model)
    {
        Collection<Especialidad> especialidades = especialidadService.listarEspecialidades();
        Doctor doctor = doctorService.obtenerDoctor(id);
        model.put("doctor", doctor);
        model.put("especialidades", especialidades);
        return "doctor/modificar";
    }

    @RequestMapping(value = {"/modificar"}, method = RequestMethod.POST)
    public String modificar(@RequestParam Integer id,
                            @RequestParam Integer especialidad,
                            @RequestParam String primernombre,
                            @RequestParam String segundonombre,
                            @RequestParam String dni,
                            @RequestParam String cmp,
                            ModelMap model)
    {
        Doctor doctor = new Doctor();
        String result = "";
        Especialidad especialidadSolicitada = especialidadService.obtenerEspecialidad(especialidad);

        doctor.setId(id);
        doctor.setPrimerNombre(primernombre);
        doctor.setSegundoNombre(segundonombre);
        doctor.setEspecialidad(especialidadSolicitada);
        doctor.setDni(dni);
        doctor.setCmp(cmp);

        doctor = doctorService.insertarDoctor(doctor);

        if (doctor != null)
        {
            result = "redirect:/doctor/resumen";
        }
        else
        {
            Collection<Especialidad> especialidades = especialidadService.listarEspecialidades();
            model.put("especialidades", especialidades);
            model.put("mensaje", "Ocurrio un error al guardar");
            return "doctor/modificar";
        }

        return result;
    }

    @PostMapping("/eliminar/{id}")
    @ResponseBody
    public ResponseEntity<Response> eliminardoctor(@PathVariable(value = "id") Integer id)
    {
        ResponseEntity<Response> response = null;
        int result = doctorService.eliminarById(id);

        if (result == 1) {
            response = new ResponseEntity<Response>(new Response("Se elimino correctamemte"),
                    HttpStatus.OK);
        } else {
            response = new ResponseEntity<Response>(new Response("No se elimino"),
                    HttpStatus.EXPECTATION_FAILED);
        }

        return response;
    }

}
