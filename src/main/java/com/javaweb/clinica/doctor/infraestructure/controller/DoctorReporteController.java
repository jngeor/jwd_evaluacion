package com.javaweb.clinica.doctor.infraestructure.controller;

import com.javaweb.clinica.consulta.application.IConsultaService;
import com.javaweb.clinica.doctor.application.IDoctorService;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/reporte")
public class DoctorReporteController {

    private static final String PATH = "src/main/resources/reportes";

    @Autowired
    IDoctorService doctorService;

    @Autowired
    IConsultaService consultaService;

    @GetMapping("/medicos")
    public void medicos(HttpServletResponse response, @RequestParam(value = "mode", required = false) String mode)
            throws JRException, IOException
    {
        byte[] data = createPDFMed();
        strearmReportMed(response, data, "medicos.pdf", mode);
    }

    @GetMapping("/historia")
    public void historia(HttpServletResponse response, @RequestParam(value = "mode", required = false) String mode)
            throws JRException, IOException
    {
        byte[] data = createPDFHis();
        strearmReportHis(response, data, "historia.pdf", mode);
    }

    private byte[] createPDFMed() throws JRException {
        JasperReport report = JasperCompileManager.compileReport(PATH + "/Medico.jrxml");
        Map<String, Object> parameters = new HashMap<>();
        JRBeanCollectionDataSource source = new JRBeanCollectionDataSource(doctorService.listarDoctorEspecialidad());
        JasperPrint print = JasperFillManager.fillReport(report, parameters, source);

        return JasperExportManager.exportReportToPdf(print);
    }

    private void strearmReportMed(HttpServletResponse response, byte[] data, String name, String mode) throws IOException {
        if(mode != null && mode.equals("inline")) {
            response.setContentType("application/pdf");
            response.setHeader("Content-disposition", "inline; filename= " + name);
        } else {
            response.setContentType("application/x-download");
            response.setHeader("Content-disposition", "attachment; filename= " + name);
        }

        response.setContentLength(data.length);
        response.getOutputStream().write(data);
        response.getOutputStream().flush();
    }

    private byte[] createPDFHis() throws JRException {
        JasperReport report = JasperCompileManager.compileReport(PATH + "/Historia.jrxml");
        Map<String, Object> parameters = new HashMap<>();
        JRBeanCollectionDataSource source = new JRBeanCollectionDataSource(consultaService.listarHistorias());
        JasperPrint print = JasperFillManager.fillReport(report, parameters, source);

        return JasperExportManager.exportReportToPdf(print);
    }

    private void strearmReportHis(HttpServletResponse response, byte[] data, String name, String mode) throws IOException {
        if(mode != null && mode.equals("inline")) {
            response.setContentType("application/pdf");
            response.setHeader("Content-disposition", "inline; filename= " + name);
        } else {
            response.setContentType("application/x-download");
            response.setHeader("Content-disposition", "attachment; filename= " + name);
        }

        response.setContentLength(data.length);
        response.getOutputStream().write(data);
        response.getOutputStream().flush();
    }

}
