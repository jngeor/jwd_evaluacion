package com.javaweb.clinica.doctor.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.javaweb.clinica.especialidad.domain.Especialidad;
import lombok.*;

import javax.persistence.*;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "doctor")
public class Doctor {

    @NonNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "serial")
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_specialty", updatable = false)
    @JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
    private Especialidad especialidad;

    @NonNull
    @Column(name = "firt_name")
    private String primerNombre;

    @NonNull
    @Column(name = "last_name")
    private String segundoNombre;

    @NonNull
    @Column(name = "dni")
    private String dni;

    @NonNull
    @Column(name = "cmp")
    private String cmp;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    @Transient
    private String nombre;

    public String getNombre()
    {
        return segundoNombre + ", " + primerNombre;
    }

}
