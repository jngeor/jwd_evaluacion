package com.javaweb.clinica.doctor.domain;

import lombok.Data;

@Data
public class DoctorEspecialidad {

    private String firt_name;
    private String last_name;
    private String dni;
    private String cmp;
    private String name;

}
