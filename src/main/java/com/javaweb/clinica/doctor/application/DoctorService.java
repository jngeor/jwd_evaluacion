package com.javaweb.clinica.doctor.application;

import com.javaweb.clinica.doctor.domain.Doctor;
import com.javaweb.clinica.doctor.domain.DoctorEspecialidad;
import com.javaweb.clinica.doctor.infraestructure.repository.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class DoctorService implements IDoctorService {

    @Autowired
    private DoctorRepository doctorRepository;


    @Override
    public Collection<Doctor> listarDoctores() {
        return doctorRepository.findAll(Sort.by("id"));
    }

    @Override
    public Doctor obtenerDoctor(int id) {
        return doctorRepository.findById(id).orElse(null);
    }

    @Override
    public Doctor insertarDoctor(Doctor doctor) {
        return doctorRepository.save(doctor);
    }

    @Override
    public Integer eliminarById(int id) {
        doctorRepository.deleteById(id);
        return 1;
    }

    @Override
    public Collection<DoctorEspecialidad> listarDoctorEspecialidad() {
        Collection<DoctorEspecialidad> lista = new ArrayList<>();
        Collection<Doctor> doctores = doctorRepository.findAll();
        doctores.forEach( doc -> {
            DoctorEspecialidad docesp = new DoctorEspecialidad();
            docesp.setFirt_name(doc.getPrimerNombre());
            docesp.setLast_name(doc.getSegundoNombre());
            docesp.setDni(doc.getDni());
            docesp.setCmp(doc.getCmp());
            docesp.setName(doc.getEspecialidad().getNombre());
            lista.add(docesp);
        });

        return lista;
    }

}
