package com.javaweb.clinica.doctor.application;

import com.javaweb.clinica.doctor.domain.Doctor;
import com.javaweb.clinica.doctor.domain.DoctorEspecialidad;

import java.util.Collection;

public interface IDoctorService {

    public Collection<Doctor> listarDoctores();
    public Doctor obtenerDoctor(int id);
    public Doctor insertarDoctor(Doctor doctor);
    public Integer eliminarById(int id);
    public Collection<DoctorEspecialidad> listarDoctorEspecialidad();

}
