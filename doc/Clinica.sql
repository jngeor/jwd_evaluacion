CREATE TABLE public.usuario
(
    id integer NOT NULL DEFAULT nextval('usuario_id_seq'::regclass),
    nombre character varying(100) COLLATE pg_catalog."default" NOT NULL,
    documento character varying(8) COLLATE pg_catalog."default" NOT NULL,
    clave character varying(100) COLLATE pg_catalog."default" NOT NULL,
    rol character varying(20) COLLATE pg_catalog."default",
    CONSTRAINT usuario_pk PRIMARY KEY (id),
    CONSTRAINT cliente_uc_documento UNIQUE (documento)
);

ALTER TABLE public.usuario
    OWNER to postgres;
	
CREATE TABLE public.specialty
(
    id integer NOT NULL DEFAULT nextval('specialty_id_seq'::regclass),
    name character varying(30) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT specialty_pkey PRIMARY KEY (id)
);

ALTER TABLE public.specialty
    OWNER to postgres;
	
CREATE TABLE public.doctor
(
    id serial NOT NULL,
    id_specialty integer NOT NULL,
    firt_name character varying(50) NOT NULL,
    last_name character varying(50) NOT NULL,
    dni character varying(8) NOT NULL,
    cmp character varying(20) NOT NULL,
    CONSTRAINT doctor_pk PRIMARY KEY (id),
    CONSTRAINT cliente_fk_specialty FOREIGN KEY (id_specialty)
        REFERENCES public.specialty (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

ALTER TABLE public.doctor
    OWNER to postgres;

CREATE TABLE public.patient
(
    id integer NOT NULL DEFAULT nextval('patient_id_seq'::regclass),
    first_name character varying(50) COLLATE pg_catalog."default" NOT NULL,
    last_name character varying(50) COLLATE pg_catalog."default" NOT NULL,
    dni character varying(8) COLLATE pg_catalog."default" NOT NULL,
    number_clinical_history character varying(20) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT patient_pkey PRIMARY KEY (id);
);


ALTER TABLE public.patient
    OWNER to postgres;	
	
CREATE TABLE public.medical_consultation
(
    id integer NOT NULL DEFAULT nextval('medical_consultation_id_seq'::regclass),
    id_doctor integer NOT NULL,
    id_patient integer NOT NULL,
    create_date date NOT NULL,
    CONSTRAINT medical_consultation_pkey PRIMARY KEY (id),
    CONSTRAINT mc_fk_doctor FOREIGN KEY (id_doctor)
        REFERENCES public.doctor (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT mc_fk_patient FOREIGN KEY (id_patient)
        REFERENCES public.patient (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

ALTER TABLE public.medical_consultation
    OWNER to postgres;	
	

CREATE TABLE public.detail_consultation
(
    id integer NOT NULL DEFAULT nextval('detail_consultation_id_seq'::regclass),
    diagnostic text COLLATE pg_catalog."default",
    treatment text COLLATE pg_catalog."default",
    id_medical_consultation integer,
    CONSTRAINT detail_consultation_pk PRIMARY KEY (id),
    CONSTRAINT dc_fk_mc FOREIGN KEY (id_medical_consultation)
        REFERENCES public.medical_consultation (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);


ALTER TABLE public.detail_consultation
    OWNER to postgres;
	
	
	